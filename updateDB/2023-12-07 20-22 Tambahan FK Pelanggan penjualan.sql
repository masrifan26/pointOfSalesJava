ALTER TABLE `penjualan_detail`
	CHANGE COLUMN `pjual_detail_penjual_id` `pjual_detail_pelanggan_id` INT(11) NULL DEFAULT NULL AFTER `pjual_detail_id`,
	ADD INDEX `pjual_detail_pelanggan_id` (`pjual_detail_pelanggan_id`),
	ADD CONSTRAINT `pjual_pelanggan_id` FOREIGN KEY (`pjual_detail_pelanggan_id`) REFERENCES `pelanggan` (`pelanggan_id`) ON UPDATE CASCADE ON DELETE CASCADE;
