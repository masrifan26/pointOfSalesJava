-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.4.27-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             12.5.0.6677
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for db_pos_uas
DROP DATABASE IF EXISTS `db_pos_uas`;
CREATE DATABASE IF NOT EXISTS `db_pos_uas` /*!40100 DEFAULT CHARACTER SET armscii8 COLLATE armscii8_bin */;
USE `db_pos_uas`;

-- Dumping structure for procedure db_pos_uas.SearchKartuStok
DROP PROCEDURE IF EXISTS `SearchKartuStok`;
DELIMITER //
CREATE PROCEDURE `SearchKartuStok`(
	IN `tglAwal` DATE,
	IN `tglAkhir` DATE
)
BEGIN

	SELECT ks.kstok_id, ks.kstok_jenis, ks.kstok_nomor_faktur, 
			IFNULL(qpj.penjualan_tanggal, qpb.pembelian_tanggal) AS tanggal_stok, ks.kstok_detail_faktur_id, ks.kstok_produk_id, 
			ks.kstok_satuan_id, ks.kstok_jumlah, ks.kstok_gudang_id, p.produk_nama, p.produk_kode, ps.satuan_nama, g.gudang_nama
	FROM kartu_stok ks
	LEFT JOIN (
		SELECT * FROM penjualan_detail
		LEFT JOIN penjualan ON penjualan.penjualan_id = penjualan_detail.pjual_detail_penjualan_id
		LEFT JOIN pelanggan ON pelanggan.pelanggan_id = penjualan.penjualan_pelanggan_id
	)qpj ON qpj.penjualan_nomor_faktur = ks.kstok_nomor_faktur AND qpj.pjual_detail_id = ks.kstok_detail_faktur_id
	LEFT JOIN (
		SELECT * FROM pembelian_detail
		LEFT JOIN pembelian ON pembelian.pembelian_id = pembelian_detail.pbeli_detail_pembelian_id
		LEFT JOIN pemasok ON pemasok.pemasok_id = pembelian.pembelian_pemasok_id
	)qpb ON qpb.pembelian_nomor_faktur = ks.kstok_nomor_faktur AND qpb.pbeli_detail_id = ks.kstok_detail_faktur_id
	LEFT JOIN produk p ON p.produk_id = ks.kstok_produk_id
	LEFT JOIN gudang g ON g.gudang_id = ks.kstok_gudang_id
	LEFT JOIN produk_satuan ps ON ps.satuan_id = ks.kstok_satuan_id
	WHERE (penjualan_tanggal BETWEEN tglAwal AND tglAkhir) || (pembelian_tanggal BETWEEN tglAwal AND tglAkhir);

END//
DELIMITER ;

/*!40103 SET TIME_ZONE=IFNULL(@OLD_TIME_ZONE, 'system') */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
