ALTER TABLE `penjualan`
	ADD COLUMN `penjualan_bayar_rp` FLOAT NULL DEFAULT NULL AFTER `penjualan_total_rp`,
	ADD COLUMN `penjualan_cara_bayar` ENUM('Tunai','Card','Transfer') NULL DEFAULT NULL AFTER `penjualan_bayar_rp`,
	ADD COLUMN `penjualan_kembalian_rp` FLOAT NULL DEFAULT NULL AFTER `penjualan_cara_bayar`;
