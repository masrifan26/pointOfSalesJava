-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.38-log - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             12.0.0.6468
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for db_pos_uas_okky
DROP DATABASE IF EXISTS `db_pos_uas`;
CREATE DATABASE IF NOT EXISTS `db_pos_uas` /*!40100 DEFAULT CHARACTER SET armscii8 COLLATE armscii8_bin */;
USE `db_pos_uas`;

-- Dumping structure for table db_pos_uas_okky.gudang
DROP TABLE IF EXISTS `gudang`;
CREATE TABLE IF NOT EXISTS `gudang` (
  `gudang_id` int(11) NOT NULL AUTO_INCREMENT,
  `gudang_nama` varchar(50) COLLATE armscii8_bin DEFAULT NULL,
  `gudang_alamat` varchar(50) COLLATE armscii8_bin DEFAULT NULL,
  `gudang_status` enum('Aktif','Tidak Aktif') COLLATE armscii8_bin DEFAULT NULL,
  PRIMARY KEY (`gudang_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=armscii8 COLLATE=armscii8_bin;

-- Dumping data for table db_pos_uas_okky.gudang: ~5 rows (approximately)
INSERT INTO `gudang` (`gudang_id`, `gudang_nama`, `gudang_alamat`, `gudang_status`) VALUES
	(1, 'Gudang  Baru', 'Jl Sukolilo', 'Aktif'),
	(2, 'Gudang Lama', 'Jl. Jember', 'Aktif'),
	(3, 'Gudang Baru', 'Surabaya', 'Aktif'),
	(4, 'Gajah Baru', 'Jember', 'Aktif'),
	(5, 'Rizal', 'sfafa', 'Aktif');

-- Dumping structure for table db_pos_uas_okky.hak_akses
DROP TABLE IF EXISTS `hak_akses`;
CREATE TABLE IF NOT EXISTS `hak_akses` (
  `hak_akses_id` int(11) NOT NULL AUTO_INCREMENT,
  `hak_akses_nama` varchar(50) COLLATE armscii8_bin DEFAULT NULL,
  `hak_akses_status` enum('Aktif','Tidak Aktif') COLLATE armscii8_bin DEFAULT NULL,
  PRIMARY KEY (`hak_akses_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=armscii8 COLLATE=armscii8_bin;

-- Dumping data for table db_pos_uas_okky.hak_akses: ~2 rows (approximately)
INSERT INTO `hak_akses` (`hak_akses_id`, `hak_akses_nama`, `hak_akses_status`) VALUES
	(1, 'Administrator', 'Aktif'),
	(2, 'Kasir', 'Aktif');

-- Dumping structure for procedure db_pos_uas_okky.InsertKaryawan
DROP PROCEDURE IF EXISTS `InsertKaryawan`;
DELIMITER //
CREATE PROCEDURE `InsertKaryawan`(
    IN karyawanNamaValue VARCHAR(255),
    IN karyawanAlamatValue VARCHAR(255),
    IN karyawanNotlpValue VARCHAR(20),
    IN karyawanJenisValue VARCHAR(10),
    IN karyawanStatusValue VARCHAR(20)
)
BEGIN
    INSERT INTO karyawan (karyawan_nama, karyawan_alamat, karyawan_tlp, karyawan_jenis_kelamin, karyawan_status)
    VALUES (karyawanNamaValue, karyawanAlamatValue, karyawanNotlpValue, karyawanJenisValue, karyawanStatusValue);
END//
DELIMITER ;

-- Dumping structure for procedure db_pos_uas_okky.InsertPelanggan
DROP PROCEDURE IF EXISTS `InsertPelanggan`;
DELIMITER //
CREATE PROCEDURE `InsertPelanggan`(
    IN pelangganNamaValue VARCHAR(255),
    IN pelangganAlamatValue VARCHAR(255),
    IN pelangganNotlpValue VARCHAR(20),
    IN pelangganJenisValue VARCHAR(10),
    IN pelangganStatusValue VARCHAR(20)
)
BEGIN
    INSERT INTO pelanggan (pelanggan_nama, pelanggan_alamat, pelanggan_tlp, pelanggan_jenis_kelamin, pelanggan_status)
    VALUES (pelangganNamaValue, pelangganAlamatValue, pelangganNotlpValue, pelangganJenisValue, pelangganStatusValue);
END//
DELIMITER ;

-- Dumping structure for procedure db_pos_uas_okky.InsertProdukSatuan
DROP PROCEDURE IF EXISTS `InsertProdukSatuan`;
DELIMITER //
CREATE PROCEDURE `InsertProdukSatuan`(
    IN satuanKodeValue VARCHAR(255),
    IN satuanNamaValue VARCHAR(255),
    IN satuanStatusValue VARCHAR(20)
)
BEGIN
    INSERT INTO produk_satuan (satuan_kode, satuan_nama, satuan_status)
    VALUES (satuanKodeValue, satuanNamaValue, satuanStatusValue);
END//
DELIMITER ;

-- Dumping structure for table db_pos_uas_okky.kartu_stok
DROP TABLE IF EXISTS `kartu_stok`;
CREATE TABLE IF NOT EXISTS `kartu_stok` (
  `kstok_id` int(11) NOT NULL AUTO_INCREMENT,
  `kstok_jenis` enum('masuk','keluar') COLLATE armscii8_bin DEFAULT NULL,
  `kstok_nomor_faktur` char(50) COLLATE armscii8_bin DEFAULT NULL,
  `kstok_detail_faktur_id` int(11) DEFAULT NULL,
  `kstok_produk_id` int(11) DEFAULT NULL,
  `kstok_satuan_id` int(11) DEFAULT NULL,
  `kstok_jumlah` float DEFAULT NULL,
  `kstok_gudang_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`kstok_id`),
  KEY `penjualan_id` (`kstok_detail_faktur_id`),
  KEY `pembelian_id` (`kstok_produk_id`),
  KEY `kstok_nomor_faktur` (`kstok_nomor_faktur`),
  CONSTRAINT `FK_kartu_stok_penjualan_produk` FOREIGN KEY (`kstok_produk_id`) REFERENCES `penjualan_detail` (`pjual_detail_produk_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=armscii8 COLLATE=armscii8_bin;

-- Dumping data for table db_pos_uas_okky.kartu_stok: ~3 rows (approximately)
INSERT INTO `kartu_stok` (`kstok_id`, `kstok_jenis`, `kstok_nomor_faktur`, `kstok_detail_faktur_id`, `kstok_produk_id`, `kstok_satuan_id`, `kstok_jumlah`, `kstok_gudang_id`) VALUES
	(6, 'masuk', 'PJ0005', 35, 2, 1, 1, 1),
	(8, 'keluar', 'PJ0011', 37, 3, 1, 10, 1),
	(9, 'keluar', 'PJ0012', 38, 5, 1, 10, 4);

-- Dumping structure for table db_pos_uas_okky.karyawan
DROP TABLE IF EXISTS `karyawan`;
CREATE TABLE IF NOT EXISTS `karyawan` (
  `karyawan_id` int(11) NOT NULL AUTO_INCREMENT,
  `karyawan_nama` varchar(50) COLLATE armscii8_bin DEFAULT NULL,
  `karyawan_alamat` varchar(50) COLLATE armscii8_bin DEFAULT NULL,
  `karyawan_tlp` char(50) COLLATE armscii8_bin DEFAULT NULL,
  `karyawan_jenis_kelamin` enum('L','P') COLLATE armscii8_bin DEFAULT NULL,
  `karyawan_status` enum('Aktif','Tidak Aktif') COLLATE armscii8_bin DEFAULT NULL,
  PRIMARY KEY (`karyawan_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=armscii8 COLLATE=armscii8_bin;

-- Dumping data for table db_pos_uas_okky.karyawan: ~9 rows (approximately)
INSERT INTO `karyawan` (`karyawan_id`, `karyawan_nama`, `karyawan_alamat`, `karyawan_tlp`, `karyawan_jenis_kelamin`, `karyawan_status`) VALUES
	(1, 'Abdul Rohman Masrifan', 'Jombang', '087863975153', 'L', 'Aktif'),
	(2, 'Labib Falah Athallah', 'Surabaya', '087815964509', 'L', 'Aktif'),
	(3, 'Roro', 'Tuban', '088225066808', 'L', 'Aktif'),
	(4, 'Okky Hendrawan', 'Surabaya', '082132269596', 'L', 'Aktif'),
	(5, 'Rifqi', 'Sby', '12345678', 'L', 'Aktif'),
	(6, 'Barli', 'Sby', '1234567', 'L', 'Aktif'),
	(7, 'Claire', 'Malaysia', '12345678', 'P', 'Aktif'),
	(8, 'Labib Gubeng', 'Sby', '123456', 'L', 'Aktif'),
	(9, 'Rizal Asu', 'Tuban', '12345678', 'L', 'Tidak Aktif');

-- Dumping structure for table db_pos_uas_okky.pelanggan
DROP TABLE IF EXISTS `pelanggan`;
CREATE TABLE IF NOT EXISTS `pelanggan` (
  `pelanggan_id` int(10) NOT NULL AUTO_INCREMENT,
  `pelanggan_nama` varchar(50) COLLATE armscii8_bin DEFAULT NULL,
  `pelanggan_alamat` varchar(50) COLLATE armscii8_bin DEFAULT NULL,
  `pelanggan_tlp` char(50) COLLATE armscii8_bin DEFAULT NULL,
  `pelanggan_jenis_kelamin` enum('L','P') COLLATE armscii8_bin DEFAULT NULL,
  `pelanggan_status` enum('Aktif','Tidak Aktif') COLLATE armscii8_bin DEFAULT NULL,
  PRIMARY KEY (`pelanggan_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=armscii8 COLLATE=armscii8_bin;

-- Dumping data for table db_pos_uas_okky.pelanggan: ~6 rows (approximately)
INSERT INTO `pelanggan` (`pelanggan_id`, `pelanggan_nama`, `pelanggan_alamat`, `pelanggan_tlp`, `pelanggan_jenis_kelamin`, `pelanggan_status`) VALUES
	(1, 'Pelanggan Umum', 'Tuban', '088834934', 'L', 'Aktif'),
	(2, 'Labib Surabaya', 'Surabaya', '088834934', 'L', 'Aktif'),
	(3, 'Okky', 'Sby', '12345678', 'L', 'Aktif'),
	(4, 'Barli', 'Sby', '12345678', 'P', 'Tidak Aktif'),
	(5, 'Sohibul', 'Sby', '12345678', 'L', 'Tidak Aktif'),
	(6, 'Jarwo', 'sby', '1234567', 'L', 'Aktif');

-- Dumping structure for table db_pos_uas_okky.pemasok
DROP TABLE IF EXISTS `pemasok`;
CREATE TABLE IF NOT EXISTS `pemasok` (
  `pemasok_id` int(11) NOT NULL AUTO_INCREMENT,
  `pemasok_nama` varchar(50) COLLATE armscii8_bin DEFAULT NULL,
  `pemasok_alamat` varchar(50) COLLATE armscii8_bin DEFAULT NULL,
  `pemasok_tlp` char(20) COLLATE armscii8_bin DEFAULT NULL,
  `pemasok_status` enum('Aktif','Tidak Aktif') COLLATE armscii8_bin DEFAULT NULL,
  PRIMARY KEY (`pemasok_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=armscii8 COLLATE=armscii8_bin;

-- Dumping data for table db_pos_uas_okky.pemasok: ~4 rows (approximately)
INSERT INTO `pemasok` (`pemasok_id`, `pemasok_nama`, `pemasok_alamat`, `pemasok_tlp`, `pemasok_status`) VALUES
	(1, 'Rizal Tuban', 'Tuban', '0082382343', 'Aktif'),
	(2, 'Okky', 'Surabaya', '02934', 'Aktif'),
	(3, 'Rohman', 'Sby', '12345678', 'Aktif'),
	(4, 'Labib', 'Sby', '12345677', 'Aktif');

-- Dumping structure for table db_pos_uas_okky.pembelian
DROP TABLE IF EXISTS `pembelian`;
CREATE TABLE IF NOT EXISTS `pembelian` (
  `pembelian_id` int(11) NOT NULL AUTO_INCREMENT,
  `pembelian_nomor_faktur` char(50) COLLATE armscii8_bin DEFAULT NULL,
  `pembelian_tanggal` date DEFAULT NULL,
  `pembelian_pemasok_id` int(11) DEFAULT NULL,
  `pembelian_total_rp` float DEFAULT NULL,
  PRIMARY KEY (`pembelian_id`),
  KEY `pemasok_id` (`pembelian_pemasok_id`),
  CONSTRAINT `pemasok_id` FOREIGN KEY (`pembelian_pemasok_id`) REFERENCES `pemasok` (`pemasok_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=armscii8 COLLATE=armscii8_bin;

-- Dumping data for table db_pos_uas_okky.pembelian: ~5 rows (approximately)
INSERT INTO `pembelian` (`pembelian_id`, `pembelian_nomor_faktur`, `pembelian_tanggal`, `pembelian_pemasok_id`, `pembelian_total_rp`) VALUES
	(1, 'PB001', '2024-01-07', 1, 375000),
	(2, 'PB002', '2024-01-07', 2, 1200000),
	(3, 'PB003', '2024-01-08', 1, 10000),
	(4, 'PB004', '2024-01-08', 1, 1200000),
	(5, 'PB005', '2024-01-08', 1, 500000);

-- Dumping structure for procedure db_pos_uas_okky.pembelianDetail
DROP PROCEDURE IF EXISTS `pembelianDetail`;
DELIMITER //
CREATE PROCEDURE `pembelianDetail`(IN cariDataParam VARCHAR(255))
BEGIN
    DECLARE formattedHarga DECIMAL(18,2);
    DECLARE formattedTotalPembelian DECIMAL(18,2);
    DECLARE formattedSubTotal DECIMAL(18,2);

    SELECT
        produk.produk_nama,
        produk.produk_kode,
        produk.produk_keterangan,
        pemasok.pemasok_nama,
        pemasok.pemasok_alamat,
        pemasok.pemasok_tlp,
        pembelian_detail.pbeli_detail_harga,
        pembelian_detail.pbeli_detail_jumlah,
        pembelian_detail.pbeli_detail_persen,
        pembelian_detail.pbeli_detail_subtotal,
        pembelian.pembelian_total_rp,
        pembelian.pembelian_nomor_faktur,
        pembelian.pembelian_tanggal
    FROM
        pembelian_detail
        LEFT JOIN produk ON pembelian_detail.pbeli_detail_produk_id = produk.produk_id
        LEFT JOIN pembelian ON pembelian_detail.pbeli_detail_pembelian_id = pembelian.pembelian_id
        LEFT JOIN pemasok ON pembelian.pembelian_pemasok_id = pemasok.pemasok_id
    WHERE
        pembelian.pembelian_id LIKE CONCAT('%', cariDataParam, '%');

    /* You can add more conditions to the WHERE clause if needed */

END//
DELIMITER ;

-- Dumping structure for procedure db_pos_uas_okky.pembelianDetailTanggal
DROP PROCEDURE IF EXISTS `pembelianDetailTanggal`;
DELIMITER //
CREATE PROCEDURE `pembelianDetailTanggal`(IN tanggalAwalParam DATE, IN tanggalAkhirParam DATE)
BEGIN
    DECLARE formattedHarga DECIMAL(18,2);
    DECLARE formattedTotalPembelian DECIMAL(18,2);
    DECLARE formattedSubTotal DECIMAL(18,2);

    SELECT
        produk.produk_nama,
        produk.produk_kode,
        produk.produk_keterangan,
        pemasok.pemasok_nama,
        pemasok.pemasok_alamat,
        pemasok.pemasok_tlp,
        pembelian_detail.pbeli_detail_harga,
        pembelian_detail.pbeli_detail_jumlah,
        pembelian_detail.pbeli_detail_persen,
        pembelian_detail.pbeli_detail_subtotal,
        pembelian.pembelian_total_rp,
        pembelian.pembelian_nomor_faktur,
        pembelian.pembelian_tanggal
    FROM
        pembelian_detail
        LEFT JOIN produk ON pembelian_detail.pbeli_detail_produk_id = produk.produk_id
        LEFT JOIN pembelian ON pembelian_detail.pbeli_detail_pembelian_id = pembelian.pembelian_id
        LEFT JOIN pemasok ON pembelian.pembelian_pemasok_id = pemasok.pemasok_id
    WHERE
        pembelian.pembelian_tanggal BETWEEN tanggalAwalParam AND tanggalAkhirParam;

    /* You can add more conditions to the WHERE clause if needed */

END//
DELIMITER ;

-- Dumping structure for procedure db_pos_uas_okky.pembelianRekap
DROP PROCEDURE IF EXISTS `pembelianRekap`;
DELIMITER //
CREATE PROCEDURE `pembelianRekap`(
	IN `cariDataParam` VARCHAR(255)
)
BEGIN
    DECLARE formattedTotalPembelian DECIMAL(18,2);
    
   SELECT
	pembelian.pembelian_nomor_faktur,
	pembelian.pembelian_tanggal,
	pembelian.pembelian_total_rp,
	pemasok.pemasok_nama,
	pemasok.pemasok_alamat,
	pemasok.pemasok_tlp
	FROM pembelian

	LEFT JOIN pemasok ON pembelian.pembelian_pemasok_id = pemasok.pemasok_id
   WHERE pembelian_id LIKE CONCAT('%', cariDataParam, '%');
    
    /* You can add more columns to the SELECT statement if needed */

END//
DELIMITER ;

-- Dumping structure for procedure db_pos_uas_okky.pembelianRekapTanggal
DROP PROCEDURE IF EXISTS `pembelianRekapTanggal`;
DELIMITER //
CREATE PROCEDURE `pembelianRekapTanggal`(
	IN `tanggalAwalParam` DATE,
	IN `tanggalAkhirParam` DATE
)
BEGIN
    DECLARE formattedTotalPembelian DECIMAL(18,2);
    
      
   SELECT
	pembelian.pembelian_nomor_faktur,
	pembelian.pembelian_tanggal,
	pembelian.pembelian_total_rp,
	pemasok.pemasok_nama,
	pemasok.pemasok_alamat,
	pemasok.pemasok_tlp
	FROM pembelian

	LEFT JOIN pemasok ON pembelian.pembelian_pemasok_id = pemasok.pemasok_id
   WHERE pembelian_tanggal BETWEEN tanggalAwalParam AND tanggalAkhirParam;
    
    /* You can add more columns to the SELECT statement if needed */

END//
DELIMITER ;

-- Dumping structure for table db_pos_uas_okky.pembelian_detail
DROP TABLE IF EXISTS `pembelian_detail`;
CREATE TABLE IF NOT EXISTS `pembelian_detail` (
  `pbeli_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `pbeli_detail_pembelian_id` int(11) DEFAULT NULL,
  `pbeli_detail_gudang_id` int(11) DEFAULT NULL,
  `pbeli_detail_produk_id` int(11) DEFAULT NULL,
  `pbeli_detail_satuan_id` int(11) DEFAULT NULL,
  `pbeli_detail_harga` float DEFAULT NULL,
  `pbeli_detail_jumlah` float DEFAULT NULL,
  `pbeli_detail_diskon_rp` float DEFAULT NULL,
  `pbeli_detail_persen` float DEFAULT NULL,
  `pbeli_detail_subtotal` float DEFAULT NULL,
  PRIMARY KEY (`pbeli_detail_id`),
  KEY `produk_id` (`pbeli_detail_produk_id`),
  KEY `satuan_id` (`pbeli_detail_satuan_id`),
  CONSTRAINT `produk_id` FOREIGN KEY (`pbeli_detail_produk_id`) REFERENCES `produk` (`produk_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `satuan_id` FOREIGN KEY (`pbeli_detail_satuan_id`) REFERENCES `produk_satuan` (`satuan_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=armscii8 COLLATE=armscii8_bin;

-- Dumping data for table db_pos_uas_okky.pembelian_detail: ~5 rows (approximately)
INSERT INTO `pembelian_detail` (`pbeli_detail_id`, `pbeli_detail_pembelian_id`, `pbeli_detail_gudang_id`, `pbeli_detail_produk_id`, `pbeli_detail_satuan_id`, `pbeli_detail_harga`, `pbeli_detail_jumlah`, `pbeli_detail_diskon_rp`, `pbeli_detail_persen`, `pbeli_detail_subtotal`) VALUES
	(1, 1, 1, 3, 1, 25000, 15, 0, 0, 375000),
	(2, 2, 1, 2, 3, 120000, 10, 0, 0, 1200000),
	(3, 3, 1, 5, 1, 1000, 10, 0, 0, 10000),
	(4, 4, 1, 2, 3, 120000, 10, 0, 0, 1200000),
	(5, 5, 1, 6, 1, 50000, 10, 0, 0, 500000);

-- Dumping structure for table db_pos_uas_okky.penjualan
DROP TABLE IF EXISTS `penjualan`;
CREATE TABLE IF NOT EXISTS `penjualan` (
  `penjualan_id` int(11) NOT NULL AUTO_INCREMENT,
  `penjualan_nomor_faktur` char(50) COLLATE armscii8_bin DEFAULT NULL,
  `penjualan_tanggal` date DEFAULT NULL,
  `penjualan_pelanggan_id` int(11) DEFAULT NULL,
  `penjualan_gudang_id` int(11) DEFAULT NULL,
  `penjualan_total_rp` float DEFAULT NULL,
  `penjualan_bayar_rp` float DEFAULT NULL,
  `penjualan_cara_bayar` enum('Tunai','Kartu','Transfer') COLLATE armscii8_bin DEFAULT NULL,
  `penjualan_kembalian_rp` float DEFAULT NULL,
  `penjualan_keterangan` varchar(250) COLLATE armscii8_bin DEFAULT NULL,
  PRIMARY KEY (`penjualan_id`),
  KEY `pelanggan_id` (`penjualan_pelanggan_id`),
  KEY `penjualan_gudang_id` (`penjualan_gudang_id`),
  CONSTRAINT `pelanggan_id` FOREIGN KEY (`penjualan_pelanggan_id`) REFERENCES `pelanggan` (`pelanggan_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=armscii8 COLLATE=armscii8_bin;

-- Dumping data for table db_pos_uas_okky.penjualan: ~12 rows (approximately)
INSERT INTO `penjualan` (`penjualan_id`, `penjualan_nomor_faktur`, `penjualan_tanggal`, `penjualan_pelanggan_id`, `penjualan_gudang_id`, `penjualan_total_rp`, `penjualan_bayar_rp`, `penjualan_cara_bayar`, `penjualan_kembalian_rp`, `penjualan_keterangan`) VALUES
	(1, 'PJ0001', '2023-12-07', 1, NULL, 200000, 200000, 'Tunai', 0, 'Tes'),
	(2, 'PJ0002', '2023-12-07', 2, NULL, 25000, 100000, 'Kartu', 50000, 'Teman'),
	(6, 'PJ0003', '2023-12-09', 1, 1, 25000, 200000, 'Tunai', 175000, 'wwww'),
	(7, 'PJ0004', '2024-01-01', 1, 1, 120000, 1000, 'Tunai', 0, ''),
	(8, 'PJ0005', '2024-01-01', 1, 1, 120000, 10, 'Tunai', 0, ''),
	(9, 'PJ0006', '2024-01-01', 1, 1, 120000, 0, 'Tunai', 0, 'www123'),
	(10, 'PJ0007', '2024-01-01', 2, 2, 1000, 0, 'Tunai', 0, 'qweqwe'),
	(11, 'PJ0008', '2024-01-01', 1, 1, 601000, 620000, 'Tunai', 19000, 'qweqwe'),
	(12, 'PJ0009', '2024-01-01', 1, 1, 76000, 80000, 'Tunai', 4000, ''),
	(13, 'PJ0010', '2024-01-01', 2, 1, 300000, 300000, 'Tunai', 0, 'www123'),
	(14, 'PJ0011', '2024-01-02', 2, 1, 250000, 300000, 'Tunai', 50000, ''),
	(15, 'PJ0012', '2024-01-08', 4, 4, 10000, 20000, 'Tunai', 10000, '');

-- Dumping structure for table db_pos_uas_okky.penjualan_detail
DROP TABLE IF EXISTS `penjualan_detail`;
CREATE TABLE IF NOT EXISTS `penjualan_detail` (
  `pjual_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `pjual_detail_penjualan_id` int(11) DEFAULT NULL,
  `pjual_detail_produk_id` int(11) DEFAULT NULL,
  `pjual_detail_satuan_id` int(11) DEFAULT NULL,
  `pjual_detail_harga` float DEFAULT NULL,
  `pjual_detail_jumlah` float DEFAULT NULL,
  `pjual_detail_diskon_rp` float DEFAULT NULL,
  `pjual_detail_diskon_persen` float DEFAULT NULL,
  `pjual_detail_subtotal` float DEFAULT NULL,
  PRIMARY KEY (`pjual_detail_id`),
  KEY `p_detail_id` (`pjual_detail_produk_id`),
  KEY `pjual_detail_pelanggan_id` (`pjual_detail_penjualan_id`) USING BTREE,
  CONSTRAINT `FK_penjualan_detail_produk` FOREIGN KEY (`pjual_detail_produk_id`) REFERENCES `produk` (`produk_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `pjual_penjualan_id` FOREIGN KEY (`pjual_detail_penjualan_id`) REFERENCES `penjualan` (`penjualan_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=armscii8 COLLATE=armscii8_bin;

-- Dumping data for table db_pos_uas_okky.penjualan_detail: ~11 rows (approximately)
INSERT INTO `penjualan_detail` (`pjual_detail_id`, `pjual_detail_penjualan_id`, `pjual_detail_produk_id`, `pjual_detail_satuan_id`, `pjual_detail_harga`, `pjual_detail_jumlah`, `pjual_detail_diskon_rp`, `pjual_detail_diskon_persen`, `pjual_detail_subtotal`) VALUES
	(1, 1, 1, 1, 200000, 1, 0, 0, 200000),
	(12, 2, 3, 1, 25000, 5, 0, 0, 25000),
	(17, 10, 5, 1, 1000, 1, 0, 0, 1000),
	(18, 11, 2, 1, 120000, 5, 0, 0, 600000),
	(19, 11, 5, 1, 1000, 1, 0, 0, 1000),
	(28, 11, 5, 1, 1000, 1, 0, 0, 1000),
	(30, 13, 3, 1, 25000, 12, 0, 0, 300000),
	(31, 6, 3, 1, 25000, 1, 0, 0, 25000),
	(35, 8, 2, 1, 120000, 1, 0, 0, 120000),
	(37, 14, 3, 1, 25000, 10, 0, 0, 250000),
	(38, 15, 5, 1, 1000, 10, 0, 0, 10000);

-- Dumping structure for table db_pos_uas_okky.produk
DROP TABLE IF EXISTS `produk`;
CREATE TABLE IF NOT EXISTS `produk` (
  `produk_id` int(11) NOT NULL AUTO_INCREMENT,
  `produk_kode` varchar(50) COLLATE armscii8_bin NOT NULL DEFAULT '0',
  `produk_nama` varchar(50) COLLATE armscii8_bin DEFAULT NULL,
  `produk_keterangan` varchar(100) COLLATE armscii8_bin DEFAULT NULL,
  `produk_kategori_id` int(11) DEFAULT NULL,
  `produk_status` enum('Aktif','Tidak Aktif') COLLATE armscii8_bin DEFAULT NULL,
  PRIMARY KEY (`produk_id`),
  KEY `FK_produk_produk_kategori` (`produk_kategori_id`),
  CONSTRAINT `kategori_id` FOREIGN KEY (`produk_kategori_id`) REFERENCES `produk_kategori` (`kategori_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=armscii8 COLLATE=armscii8_bin;

-- Dumping data for table db_pos_uas_okky.produk: ~7 rows (approximately)
INSERT INTO `produk` (`produk_id`, `produk_kode`, `produk_nama`, `produk_keterangan`, `produk_kategori_id`, `produk_status`) VALUES
	(1, 'CT001', 'Cat tembok2 Nip', 'wwq12', 2, 'Aktif'),
	(2, 'RK016', 'Cat tembok', 'wwq12', 3, 'Aktif'),
	(3, 'RK015', 'Surya', 'Surya 12 GGaa2', 3, 'Aktif'),
	(5, 'BG001', 'Kursi Lipat', 'Ojok ww talah', 1, 'Aktif'),
	(6, 'BG002', 'Besi Muda', 'Keren ', 1, 'Aktif'),
	(7, 'BG003', 'Besi Tua', '', 1, 'Aktif'),
	(8, 'BG004', 'Genteng Alus', 'Keren dan nyaman', 1, 'Aktif');

-- Dumping structure for table db_pos_uas_okky.produk_detail
DROP TABLE IF EXISTS `produk_detail`;
CREATE TABLE IF NOT EXISTS `produk_detail` (
  `pdetail_id` int(11) NOT NULL AUTO_INCREMENT,
  `pdetail_produk_id` int(11) DEFAULT NULL,
  `pdetail_satuan_id` int(11) DEFAULT NULL,
  `pdetail_harga` float DEFAULT NULL,
  `pdetail_nilai` float DEFAULT NULL,
  `pdetail_default` enum('Ya','Tidak') COLLATE armscii8_bin DEFAULT NULL,
  PRIMARY KEY (`pdetail_id`),
  KEY `satuan_id_fk` (`pdetail_satuan_id`),
  CONSTRAINT `satuan_id_fk` FOREIGN KEY (`pdetail_satuan_id`) REFERENCES `produk_satuan` (`satuan_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=armscii8 COLLATE=armscii8_bin;

-- Dumping data for table db_pos_uas_okky.produk_detail: ~8 rows (approximately)
INSERT INTO `produk_detail` (`pdetail_id`, `pdetail_produk_id`, `pdetail_satuan_id`, `pdetail_harga`, `pdetail_nilai`, `pdetail_default`) VALUES
	(1, 1, 1, 1000, 1, 'Ya'),
	(18, 3, 1, 25000, 1, 'Ya'),
	(24, 2, 1, 5000, 1, 'Tidak'),
	(25, 2, 3, 120000, 12, 'Ya'),
	(27, 6, 1, 50000, 1, 'Ya'),
	(28, 7, 3, 10000, 1, 'Ya'),
	(29, 8, 10, 10000, 1, 'Ya'),
	(30, 5, 1, 1000, 1, 'Ya');

-- Dumping structure for table db_pos_uas_okky.produk_kategori
DROP TABLE IF EXISTS `produk_kategori`;
CREATE TABLE IF NOT EXISTS `produk_kategori` (
  `kategori_id` int(11) NOT NULL AUTO_INCREMENT,
  `kategori_kode` char(5) COLLATE armscii8_bin DEFAULT NULL,
  `kategori_nama` varchar(50) COLLATE armscii8_bin DEFAULT NULL,
  `kategori_status` enum('Aktif','Tidak Aktif') COLLATE armscii8_bin DEFAULT NULL,
  PRIMARY KEY (`kategori_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=armscii8 COLLATE=armscii8_bin;

-- Dumping data for table db_pos_uas_okky.produk_kategori: ~4 rows (approximately)
INSERT INTO `produk_kategori` (`kategori_id`, `kategori_kode`, `kategori_nama`, `kategori_status`) VALUES
	(1, 'BG', 'Bangunan', 'Aktif'),
	(2, 'CT', 'Cat', 'Aktif'),
	(3, 'RK', 'Rokok', 'Aktif'),
	(4, 'ww', 'wempak', 'Aktif');

-- Dumping structure for table db_pos_uas_okky.produk_satuan
DROP TABLE IF EXISTS `produk_satuan`;
CREATE TABLE IF NOT EXISTS `produk_satuan` (
  `satuan_id` int(11) NOT NULL AUTO_INCREMENT,
  `satuan_kode` char(5) COLLATE armscii8_bin DEFAULT NULL,
  `satuan_nama` varchar(100) COLLATE armscii8_bin DEFAULT NULL,
  `satuan_status` enum('Aktif','Tidak Aktif') COLLATE armscii8_bin DEFAULT NULL,
  PRIMARY KEY (`satuan_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=armscii8 COLLATE=armscii8_bin;

-- Dumping data for table db_pos_uas_okky.produk_satuan: ~10 rows (approximately)
INSERT INTO `produk_satuan` (`satuan_id`, `satuan_kode`, `satuan_nama`, `satuan_status`) VALUES
	(1, 'PJ010', 'Tas', 'Aktif'),
	(2, 'PJ008', 'Stick', 'Aktif'),
	(3, 'PJ12', 'Meja', 'Aktif'),
	(4, 'PJ005', 'CETAK', 'Aktif'),
	(5, 'PJ001', 'Genteng', 'Aktif'),
	(6, 'PJ002', 'Gelas', 'Aktif'),
	(7, 'PJ004', 'Mouse', 'Aktif'),
	(8, 'PJ009', 'Kursi Lipat', 'Aktif'),
	(9, 'PJ011', 'Rokok', 'Aktif'),
	(10, 'PJ012', 'Tas Bagus', 'Aktif');

-- Dumping structure for procedure db_pos_uas_okky.SearchKaryawan
DROP PROCEDURE IF EXISTS `SearchKaryawan`;
DELIMITER //
CREATE PROCEDURE `SearchKaryawan`(IN cariData VARCHAR(255))
BEGIN
    IF NOT cariData IS NULL AND NOT cariData = '' THEN
        SELECT 
            karyawan_id,
            karyawan_nama,
            karyawan_alamat,
            karyawan_tlp,
            karyawan_jenis_kelamin,
            karyawan_status
        FROM karyawan
        WHERE karyawan_nama LIKE CONCAT('%', cariData, '%')
           OR karyawan_status = cariData
           OR karyawan_jenis_kelamin = cariData;
    ELSE
        SELECT 
            karyawan_id,
            karyawan_nama,
            karyawan_alamat,
            karyawan_tlp,
            karyawan_jenis_kelamin,
            karyawan_status
        FROM karyawan;
    END IF;
END//
DELIMITER ;

-- Dumping structure for procedure db_pos_uas_okky.SearchPelanggan
DROP PROCEDURE IF EXISTS `SearchPelanggan`;
DELIMITER //
CREATE PROCEDURE `SearchPelanggan`(IN cariData VARCHAR(255))
BEGIN
    IF NOT cariData IS NULL AND NOT cariData = '' THEN
        SELECT 
            pelanggan_id,
            pelanggan_nama,
            pelanggan_alamat,
            pelanggan_tlp,
            pelanggan_jenis_kelamin,
            pelanggan_status
        FROM pelanggan
        WHERE pelanggan_nama LIKE CONCAT('%', cariData, '%')
           OR pelanggan_status = cariData
           OR pelanggan_jenis_kelamin = cariData;
    ELSE
        SELECT 
            pelanggan_id,
            pelanggan_nama,
            pelanggan_alamat,
            pelanggan_tlp,
            pelanggan_jenis_kelamin,
            pelanggan_status
        FROM pelanggan;
    END IF;
END//
DELIMITER ;

-- Dumping structure for procedure db_pos_uas_okky.SearchProdukSatuan
DROP PROCEDURE IF EXISTS `SearchProdukSatuan`;
DELIMITER //
CREATE PROCEDURE `SearchProdukSatuan`(
    IN cariDataValue VARCHAR(255)
)
BEGIN
    IF cariDataValue IS NOT NULL AND cariDataValue != '' THEN
        SELECT * FROM produk_satuan WHERE satuan_nama LIKE CONCAT('%', cariDataValue, '%');
    ELSE
        SELECT * FROM produk_satuan;
    END IF;
END//
DELIMITER ;

-- Dumping structure for procedure db_pos_uas_okky.UpdateKaryawan
DROP PROCEDURE IF EXISTS `UpdateKaryawan`;
DELIMITER //
CREATE PROCEDURE `UpdateKaryawan`(
    IN karyawanNamaValue VARCHAR(255),
    IN karyawanAlamatValue VARCHAR(255),
    IN karyawanNotlpValue VARCHAR(20),
    IN karyawanJenisValue VARCHAR(10),
    IN karyawanStatusValue VARCHAR(20),
    IN idKaryawan INT
)
BEGIN
    UPDATE karyawan
    SET
        karyawan_nama = karyawanNamaValue,
        karyawan_alamat = karyawanAlamatValue,
        karyawan_tlp = karyawanNotlpValue,
        karyawan_jenis_kelamin = karyawanJenisValue,
        karyawan_status = karyawanStatusValue
    WHERE karyawan_id = idKaryawan;
END//
DELIMITER ;

-- Dumping structure for procedure db_pos_uas_okky.UpdatePelanggan
DROP PROCEDURE IF EXISTS `UpdatePelanggan`;
DELIMITER //
CREATE PROCEDURE `UpdatePelanggan`(
    IN pelangganNamaValue VARCHAR(255),
    IN pelangganAlamatValue VARCHAR(255),
    IN pelangganNotlpValue VARCHAR(20),
    IN pelangganJenisValue VARCHAR(10),
    IN pelangganStatusValue VARCHAR(20),
    IN idPelanggan INT
)
BEGIN
    UPDATE pelanggan
    SET
        pelanggan_nama = pelangganNamaValue,
        pelanggan_alamat = pelangganAlamatValue,
        pelanggan_tlp = pelangganNotlpValue,
        pelanggan_jenis_kelamin = pelangganJenisValue,
        pelanggan_status = pelangganStatusValue
    WHERE pelanggan_id = idPelanggan;
END//
DELIMITER ;

-- Dumping structure for procedure db_pos_uas_okky.UpdateProdukSatuan
DROP PROCEDURE IF EXISTS `UpdateProdukSatuan`;
DELIMITER //
CREATE PROCEDURE `UpdateProdukSatuan`(
    IN satuanKodeValue VARCHAR(255),
    IN satuanNamaValue VARCHAR(255),
    IN satuanStatusValue VARCHAR(20),
    IN satuanIdValue INT
)
BEGIN
    UPDATE produk_satuan
    SET satuan_kode = satuanKodeValue,
        satuan_nama = satuanNamaValue,
        satuan_status = satuanStatusValue
    WHERE satuan_id = satuanIdValue;
END//
DELIMITER ;

-- Dumping structure for table db_pos_uas_okky.user
DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_karyawan_id` int(11) DEFAULT NULL,
  `user_username` varchar(50) COLLATE armscii8_bin DEFAULT NULL,
  `user_password` varchar(50) COLLATE armscii8_bin DEFAULT NULL,
  `user_akses_id` int(11) DEFAULT NULL,
  `user_status` enum('Aktif','Tidak Aktif') COLLATE armscii8_bin DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  KEY `karyawan_id` (`user_karyawan_id`),
  KEY `hak_akses_id` (`user_akses_id`),
  CONSTRAINT `hak_akses_id` FOREIGN KEY (`user_akses_id`) REFERENCES `hak_akses` (`hak_akses_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `karyawan_id` FOREIGN KEY (`user_karyawan_id`) REFERENCES `karyawan` (`karyawan_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=armscii8 COLLATE=armscii8_bin;

-- Dumping data for table db_pos_uas_okky.user: ~5 rows (approximately)
INSERT INTO `user` (`user_id`, `user_karyawan_id`, `user_username`, `user_password`, `user_akses_id`, `user_status`) VALUES
	(1, 1, 'rohman', 'e10adc3949ba59abbe56e057f20f883e', 1, 'Aktif'),
	(2, 2, 'labib', 'e10adc3949ba59abbe56e057f20f883e', 1, 'Aktif'),
	(3, 3, 'rizal', 'e10adc3949ba59abbe56e057f20f883e', 1, 'Aktif'),
	(4, 4, 'okky', 'e10adc3949ba59abbe56e057f20f883e', 1, 'Aktif'),
	(5, 1, 'kasir', 'e10adc3949ba59abbe56e057f20f883e', 2, 'Aktif');

-- Dumping structure for trigger db_pos_uas_okky.pembelian_detail_aft_insert
DROP TRIGGER IF EXISTS `pembelian_detail_aft_insert`;
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `pembelian_detail_aft_insert` AFTER INSERT ON `pembelian_detail` FOR EACH ROW INSERT INTO kartu_stok

		(kstok_jenis, kstok_nomor_faktur, kstok_detail_faktur_id, kstok_produk_id, kstok_satuan_id, kstok_jumlah, kstok_gudang_id)  

	SELECT 

		'masuk', m.pembelian_nomor_faktur, d.pbeli_detail_id, d.pbeli_detail_produk_id, d.pbeli_detail_satuan_id, d.pbeli_detail_jumlah, m.pembelian_gudang_id

	FROM pembelian_detail d

	LEFT JOIN pembelian m ON (m.pembelian_id = d.pbeli_detail_pembelian_id)

	WHERE 

		d.pbeli_detail_id = NEW.pbeli_detail_id//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger db_pos_uas_okky.pembelian_detail_aft_update
DROP TRIGGER IF EXISTS `pembelian_detail_aft_update`;
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `pembelian_detail_aft_update` AFTER UPDATE ON `pembelian_detail` FOR EACH ROW BEGIN
		DELETE FROM kartu_stok WHERE kstok_detail_faktur_id = OLD.pbeli_detail_id;
	
	INSERT INTO kartu_stok

		(kstok_jenis, kstok_nomor_faktur, kstok_detail_faktur_id, kstok_produk_id, kstok_satuan_id, kstok_jumlah, kstok_gudang_id)  

	SELECT 

		'masuk', m.pembelian_nomor_faktur, d.pbeli_detail_id, d.pbeli_detail_produk_id, d.pbeli_detail_satuan_id, d.pbeli_detail_jumlah, m.pembelian_gudang_id

	FROM pembelian_detail d

	LEFT JOIN pembelian m ON (m.pembelian_id = d.pbeli_detail_pembelian_id)

	WHERE 

		d.pbeli_detail_id = NEW.pbeli_detail_id; 	
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger db_pos_uas_okky.penjualan_detail_aft_insert
DROP TRIGGER IF EXISTS `penjualan_detail_aft_insert`;
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `penjualan_detail_aft_insert` AFTER INSERT ON `penjualan_detail` FOR EACH ROW BEGIN
	
	INSERT INTO kartu_stok

		(kstok_jenis, kstok_nomor_faktur, kstok_detail_faktur_id, kstok_produk_id, kstok_satuan_id, kstok_jumlah, kstok_gudang_id)  

	SELECT 

		'keluar', m.penjualan_nomor_faktur, d.pjual_detail_id, d.pjual_detail_produk_id, d.pjual_detail_satuan_id, d.pjual_detail_jumlah, m.penjualan_gudang_id

	FROM penjualan_detail d

	LEFT JOIN penjualan m ON (m.penjualan_id = d.pjual_detail_penjualan_id)

	WHERE 

		d.pjual_detail_id = NEW.pjual_detail_id; 	

END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger db_pos_uas_okky.penjualan_detail_aft_update
DROP TRIGGER IF EXISTS `penjualan_detail_aft_update`;
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `penjualan_detail_aft_update` AFTER UPDATE ON `penjualan_detail` FOR EACH ROW BEGIN

	DELETE FROM kartu_stok WHERE kstok_detail_faktur_id = OLD.pjual_detail_id;
	
	INSERT INTO kartu_stok

		(kstok_jenis, kstok_nomor_faktur, kstok_detail_faktur_id, kstok_produk_id, kstok_satuan_id, kstok_jumlah, kstok_gudang_id)  

	SELECT 

		'keluar', m.penjualan_nomor_faktur, d.pjual_detail_id, d.pjual_detail_produk_id, d.pjual_detail_satuan_id, d.pjual_detail_jumlah, m.penjualan_gudang_id

	FROM penjualan_detail d

	LEFT JOIN penjualan m ON (m.penjualan_id = d.pjual_detail_penjualan_id)

	WHERE 

		d.pjual_detail_id = NEW.pjual_detail_id; 	

END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

/*!40103 SET TIME_ZONE=IFNULL(@OLD_TIME_ZONE, 'system') */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
