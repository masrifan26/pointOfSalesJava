create database prak_informatika;

use prak_informatika;

create table mahasiswa (
	nbi varchar(20) primary key not null,
    nama_mahasiswa varchar(50)
)Engine = InnoDB;

create table tugas_akhir (
	kode_ta varchar(20) primary key not null,
    nbi_mahasiswa varchar(20) not null,
    judul_ta varchar(100),
    tanggal_sidang timestamp,
    constraint fk_tugas_akhir_mahasiswa foreign key (nbi_mahasiswa) references mahasiswa (nbi)
)Engine = InnoDB;

insert into mahasiswa (nbi, nama_mahasiswa) values ('1462200221', 'Muhaimin');
insert into mahasiswa (nbi, nama_mahasiswa) values ('1462200222', 'Iskar');
insert into mahasiswa (nbi, nama_mahasiswa) values ('1462200223', 'Iswoyo');
insert into mahasiswa (nbi, nama_mahasiswa) values ('1462200224', 'Karni');
insert into mahasiswa (nbi, nama_mahasiswa) values ('1462200225', 'Awang');
insert into mahasiswa (nbi) values ('1462200226');

insert into tugas_akhir (kode_ta, nbi_mahasiswa, judul_ta, tanggal_sidang) values ('SI001', '1462200221', 'SISTEM DIGITAL', '2023-11-20');

insert into tugas_akhir (kode_ta, nbi_mahasiswa, judul_ta, tanggal_sidang) values ('SI002', '1462200222', 'MBD', '2023-11-15');

insert into tugas_akhir (kode_ta, nbi_mahasiswa, judul_ta, tanggal_sidang) values ('SI003', '1462200223', 'SISTEM INFORMASI', '2023-11-10');

insert into tugas_akhir (kode_ta, nbi_mahasiswa, judul_ta, tanggal_sidang) values ('SI004', '1462200224', ' ', '2023-11-15');

insert into tugas_akhir (kode_ta, judul_ta, tanggal_sidang) values ('SI005', '1462200225', '2023-11-10');

insert into tugas_akhir (kode_ta, nbi_mahasiswa, judul_ta, tanggal_sidang) values ('SI006', '1462200226', 'SISJARKOM', '2023-11-30');

select * from tugas_akhir;

select tugas_akhir.kode_ta, mahasiswa.nbi, mahasiswa.nama_mahasiswa, tugas_akhir.judul_ta, tugas_akhir.tanggal_sidang
from tugas_akhir
join mahasiswa on (mahasiswa.nbi = tugas_akhir.nbi_mahasiswa)
order by tugas_akhir.tanggal_sidang ASC;

select tugas_akhir.kode_ta, mahasiswa.nbi, mahasiswa.nama_mahasiswa, tugas_akhir.judul_ta, tugas_akhir.tanggal_sidang
from tugas_akhir
left join mahasiswa on (mahasiswa.nbi = tugas_akhir.nbi_mahasiswa);

select tugas_akhir.kode_ta, mahasiswa.nbi, mahasiswa.nama_mahasiswa, tugas_akhir.judul_ta, tugas_akhir.tanggal_sidang
from tugas_akhir
Right join mahasiswa on (mahasiswa.nbi = tugas_akhir.nbi_mahasiswa);

select tugas_akhir.kode_ta, mahasiswa.nbi, mahasiswa.nama_mahasiswa, tugas_akhir.judul_ta, tugas_akhir.tanggal_sidang
from tugas_akhir
left join mahasiswa on (mahasiswa.nbi = tugas_akhir.nbi_mahasiswa)
union
select tugas_akhir.kode_ta, mahasiswa.nbi, mahasiswa.nama_mahasiswa, tugas_akhir.judul_ta, tugas_akhir.tanggal_sidang
from tugas_akhir
right join mahasiswa on (mahasiswa.nbi = tugas_akhir.nbi_mahasiswa);
