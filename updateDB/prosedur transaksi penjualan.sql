-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.4.32-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             12.6.0.6765
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for db_pos_uas
CREATE DATABASE IF NOT EXISTS `db_pos_uas` /*!40100 DEFAULT CHARACTER SET armscii8 COLLATE armscii8_bin */;
USE `db_pos_uas`;

-- Dumping structure for procedure db_pos_uas.SearchTransaksiPenjualan
DELIMITER //
CREATE PROCEDURE `SearchTransaksiPenjualan`(
	IN `cariData` VARCHAR(255)
)
BEGIN
    IF cariData IS NOT NULL THEN
        SELECT
            pj.penjualan_id,
            p.pelanggan_id,
            pj.penjualan_tanggal,
            pj.penjualan_nomor_faktur,
            p.pelanggan_nama,
            pj.penjualan_cara_bayar,
            pj.penjualan_total_rp,
            pj.penjualan_bayar_rp,
            pj.penjualan_kembalian_rp,
            pj.penjualan_keterangan
        FROM penjualan pj
        LEFT JOIN pelanggan p ON p.pelanggan_id = pj.penjualan_pelanggan_id
        WHERE pj.penjualan_nomor_faktur LIKE CONCAT('%', cariData, '%')
            OR p.pelanggan_nama LIKE CONCAT('%', cariData, '%')
            OR pj.penjualan_cara_bayar LIKE CONCAT('%', cariData, '%');
    ELSE
        SELECT
            pj.penjualan_id,
            pj.penjualan_tanggal,
            pj.penjualan_nomor_faktur,
            p.pelanggan_id,
            p.pelanggan_nama,
            pj.penjualan_cara_bayar,
            pj.penjualan_total_rp,
            pj.penjualan_bayar_rp,
            pj.penjualan_kembalian_rp,
            pj.penjualan_keterangan
        FROM penjualan pj
        LEFT JOIN pelanggan p ON p.pelanggan_id = pj.penjualan_pelanggan_id;
    END IF;
END//
DELIMITER ;

-- Dumping structure for procedure db_pos_uas.TransaksiPenjualanDetailDelete
DELIMITER //
CREATE PROCEDURE `TransaksiPenjualanDetailDelete`(IN penjualan_id_value INT)
BEGIN
    DELETE FROM penjualan_detail WHERE pjual_detail_penjualan_id = penjualan_id_value;
END//
DELIMITER ;

-- Dumping structure for procedure db_pos_uas.TransaksiPenjualanDetailInsert
DELIMITER //
CREATE PROCEDURE `TransaksiPenjualanDetailInsert`(
    IN penjualan_id_value INT,
    IN produk_id_value INT,
    IN satuan_id_value INT,
    IN harga_value VARCHAR(255),
    IN jumlah_value VARCHAR(255),
    IN disk_rp_value VARCHAR(255),
    IN disk_persen_value VARCHAR(255),
    IN subtotal_rp_value VARCHAR(255)
)
BEGIN
    INSERT INTO penjualan_detail (
        pjual_detail_penjualan_id,
        pjual_detail_produk_id,
        pjual_detail_satuan_id,
        pjual_detail_harga,
        pjual_detail_jumlah,
        pjual_detail_diskon_rp,
        pjual_detail_diskon_persen,
        pjual_detail_subtotal
    )
    VALUES (
        penjualan_id_value,
        produk_id_value,
        satuan_id_value,
        harga_value,
        jumlah_value,
        disk_rp_value,
        disk_persen_value,
        subtotal_rp_value
    );
END//
DELIMITER ;

-- Dumping structure for procedure db_pos_uas.TransaksiPenjualanInsert
DELIMITER //
CREATE PROCEDURE `TransaksiPenjualanInsert`(
	IN `nomor_faktur` VARCHAR(255),
	IN `tanggal_penjualan` DATE,
	IN `pelanggan_id` INT,
	IN `gudang_id` INT,
	IN `total_rp` VARCHAR(255),
	IN `bayar_rp` VARCHAR(255),
	IN `cara_bayar` VARCHAR(255),
	IN `kembalian_rp` VARCHAR(255),
	IN `keterangan` VARCHAR(255)
)
BEGIN
    INSERT INTO penjualan (
        penjualan_nomor_faktur,
        penjualan_tanggal,
        penjualan_pelanggan_id,
        penjualan_gudang_id,
        penjualan_total_rp,
        penjualan_bayar_rp,
        penjualan_cara_bayar,
        penjualan_kembalian_rp,
        penjualan_keterangan
    ) VALUES (
        nomor_faktur,
        tanggal_penjualan,
        pelanggan_id,
        gudang_id,
        total_rp,
        bayar_rp,
        cara_bayar,
        kembalian_rp,
        keterangan
    );
END//
DELIMITER ;

-- Dumping structure for procedure db_pos_uas.TransaksiPenjualanKodeOtomatis
DELIMITER //
CREATE PROCEDURE `TransaksiPenjualanKodeOtomatis`()
BEGIN
    DECLARE next_kode VARCHAR(255);
    SET next_kode = (SELECT CONCAT('PJ', LPAD(IFNULL(MAX(CAST(SUBSTRING(penjualan_nomor_faktur, 3) AS UNSIGNED)), 0) + 1, 4, '0')) FROM penjualan);

    IF next_kode IS NULL THEN
        SET next_kode = 'PJ0001';
    END IF;

    SELECT next_kode AS next_code;
END//
DELIMITER ;

-- Dumping structure for procedure db_pos_uas.TransaksiPenjualanLoadDataList
DELIMITER //
CREATE PROCEDURE `TransaksiPenjualanLoadDataList`(
	IN `penjualanIdParam` VARCHAR(255)
)
BEGIN
    SELECT 
        pd.pjual_detail_id,
        pd.pjual_detail_produk_id,
        pd.pjual_detail_satuan_id,
        p.produk_nama,
        ps.satuan_nama,
        pd.pjual_detail_harga,
        pd.pjual_detail_jumlah,
        pd.pjual_detail_diskon_rp,
        pd.pjual_detail_diskon_persen,
        pd.pjual_detail_subtotal
    FROM penjualan_detail pd
    LEFT JOIN produk p ON p.produk_id = pd.pjual_detail_produk_id
    LEFT JOIN produk_satuan ps ON ps.satuan_id = pd.pjual_detail_satuan_id
    WHERE pd.pjual_detail_penjualan_id = penjualanIdParam;

END//
DELIMITER ;

-- Dumping structure for procedure db_pos_uas.TransaksiPenjualanStoreGudang
DELIMITER //
CREATE PROCEDURE `TransaksiPenjualanStoreGudang`()
BEGIN
    SELECT gudang_id, gudang_nama FROM gudang;
END//
DELIMITER ;

-- Dumping structure for procedure db_pos_uas.TransaksiPenjualanStorePelanggan
DELIMITER //
CREATE PROCEDURE `TransaksiPenjualanStorePelanggan`()
BEGIN
    SELECT pelanggan_id, pelanggan_nama FROM pelanggan;
END//
DELIMITER ;

-- Dumping structure for procedure db_pos_uas.TransaksiPenjualanStoreProduk
DELIMITER //
CREATE PROCEDURE `TransaksiPenjualanStoreProduk`()
BEGIN
    SELECT produk_id, produk_nama FROM produk;
END//
DELIMITER ;

-- Dumping structure for procedure db_pos_uas.TransaksiPenjualanStoreSatuan
DELIMITER //
CREATE PROCEDURE `TransaksiPenjualanStoreSatuan`(IN produkNamaParam VARCHAR(255))
BEGIN
    SELECT ps.satuan_id, ps.satuan_nama, pd.pdetail_harga, pd.pdetail_default
    FROM produk_satuan ps
    LEFT JOIN produk_detail pd ON pd.pdetail_satuan_id = ps.satuan_id
    LEFT JOIN produk p ON p.produk_id = pd.pdetail_produk_id
    WHERE p.produk_nama = produkNamaParam
    ORDER BY pd.pdetail_default DESC;
END//
DELIMITER ;

-- Dumping structure for procedure db_pos_uas.TransaksiPenjualanUpdate
DELIMITER //
CREATE PROCEDURE `TransaksiPenjualanUpdate`(
    IN nomor_faktur VARCHAR(255),
    IN tanggal_penjualan DATE,
    IN pelanggan_id INT,
    IN gudang_id INT,
    IN total_rp VARCHAR(255),
    IN bayar_rp VARCHAR(255),
    IN cara_bayar VARCHAR(255),
    IN kembalian_rp VARCHAR(255),
    IN keterangan VARCHAR(255),
    IN penjualan_id_update INT
)
BEGIN
    UPDATE penjualan
    SET
        penjualan_nomor_faktur = nomor_faktur,
        penjualan_tanggal = tanggal_penjualan,
        penjualan_pelanggan_id = pelanggan_id,
        penjualan_gudang_id = gudang_id,
        penjualan_total_rp = total_rp,
        penjualan_bayar_rp = bayar_rp,
        penjualan_cara_bayar = cara_bayar,
        penjualan_kembalian_rp = kembalian_rp,
        penjualan_keterangan = keterangan
    WHERE
        penjualan_id = penjualan_id_update;
END//
DELIMITER ;

/*!40103 SET TIME_ZONE=IFNULL(@OLD_TIME_ZONE, 'system') */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
