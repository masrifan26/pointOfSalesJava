-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.4.32-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             12.6.0.6765
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for db_pos_uas
CREATE DATABASE IF NOT EXISTS `db_pos_uas` /*!40100 DEFAULT CHARACTER SET armscii8 COLLATE armscii8_bin */;
USE `db_pos_uas`;

-- Dumping structure for procedure db_pos_uas.SearchPembelianDetail
DELIMITER //
CREATE PROCEDURE `SearchPembelianDetail`(
    IN searchKeyword VARCHAR(255)
)
BEGIN
    SELECT
        produk.produk_nama,
        produk.produk_kode,
        produk.produk_keterangan,
        pemasok.pemasok_nama,
        pemasok.pemasok_alamat,
        pemasok.pemasok_tlp,
        pembelian_detail.pbeli_detail_harga,
        pembelian_detail.pbeli_detail_jumlah,
        pembelian_detail.pbeli_detail_persen,
        pembelian_detail.pbeli_detail_subtotal,
        pembelian.pembelian_total_rp,
        pembelian.pembelian_nomor_faktur,
        pembelian.pembelian_tanggal
    FROM
        pembelian_detail
    LEFT JOIN
        produk ON pembelian_detail.pbeli_detail_produk_id = produk.produk_id
    LEFT JOIN
        pembelian ON pembelian_detail.pbeli_detail_pembelian_id = pembelian.pembelian_id
    LEFT JOIN
        pemasok ON pembelian.pembelian_pemasok_id = pemasok.pemasok_id
    WHERE
        produk.produk_nama LIKE CONCAT('%', searchKeyword, '%')
        OR pemasok.pemasok_nama LIKE CONCAT('%', searchKeyword, '%');
END//
DELIMITER ;

-- Dumping structure for procedure db_pos_uas.SearchPembelianRekap
DELIMITER //
CREATE PROCEDURE `SearchPembelianRekap`(
    IN searchKeyword VARCHAR(255)
)
BEGIN
    SELECT
        pembelian.pembelian_nomor_faktur,
        pembelian.pembelian_tanggal,
        pembelian.pembelian_total_rp,
        pemasok.pemasok_nama,
        pemasok.pemasok_alamat,
        pemasok.pemasok_tlp
    FROM pembelian
    LEFT JOIN pemasok ON pembelian.pembelian_pemasok_id = pemasok.pemasok_id
    WHERE pemasok.pemasok_nama LIKE CONCAT('%', searchKeyword, '%');
END//
DELIMITER ;

/*!40103 SET TIME_ZONE=IFNULL(@OLD_TIME_ZONE, 'system') */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
