-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.4.27-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             12.5.0.6677
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for db_pos_uas
DROP DATABASE IF EXISTS `db_pos_uas`;
CREATE DATABASE IF NOT EXISTS `db_pos_uas` /*!40100 DEFAULT CHARACTER SET armscii8 COLLATE armscii8_bin */;
USE `db_pos_uas`;

-- Dumping structure for procedure db_pos_uas.penjualanRekap
DROP PROCEDURE IF EXISTS `penjualanRekap`;
DELIMITER //
CREATE PROCEDURE `penjualanRekap`(
	IN `p_tanggalAwal` DATE,
	IN `p_tanggalAkhir` DATE
)
BEGIN
	SELECT 
        pj.penjualan_id,
        p.pelanggan_id,
        pj.penjualan_nomor_faktur,
        pj.penjualan_tanggal,
        p.pelanggan_nama,
        pj.penjualan_cara_bayar,
        pj.penjualan_total_rp,
        pj.penjualan_bayar_rp,
        pj.penjualan_kembalian_rp,
        pj.penjualan_keterangan
   FROM penjualan pj
   LEFT JOIN pelanggan p ON p.pelanggan_id = pj.penjualan_pelanggan_id
   WHERE pj.penjualan_tanggal BETWEEN p_tanggalAwal AND p_tanggalAkhir;
END//
DELIMITER ;

/*!40103 SET TIME_ZONE=IFNULL(@OLD_TIME_ZONE, 'system') */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
