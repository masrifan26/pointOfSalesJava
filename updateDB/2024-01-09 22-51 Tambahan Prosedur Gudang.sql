-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.4.27-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             12.5.0.6677
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for db_pos_uas
DROP DATABASE IF EXISTS `db_pos_uas`;
CREATE DATABASE IF NOT EXISTS `db_pos_uas` /*!40100 DEFAULT CHARACTER SET armscii8 COLLATE armscii8_bin */;
USE `db_pos_uas`;

-- Dumping structure for procedure db_pos_uas.insertGudang
DROP PROCEDURE IF EXISTS `insertGudang`;
DELIMITER //
CREATE PROCEDURE `insertGudang`(
	IN `p_gudang_nama` VARCHAR(225),
	IN `p_gudang_alamat` VARCHAR(225),
	IN `p_gudang_status` VARCHAR(225)
)
BEGIN
	INSERT INTO gudang (gudang_nama, gudang_alamat, gudang_status)
   VALUES (p_gudang_nama, p_gudang_alamat, p_gudang_status);
END//
DELIMITER ;

-- Dumping structure for procedure db_pos_uas.searchGudang
DROP PROCEDURE IF EXISTS `searchGudang`;
DELIMITER //
CREATE PROCEDURE `searchGudang`(
	IN `p_cariData` VARCHAR(225)
)
BEGIN
	IF p_cariData IS NOT NULL AND p_cariData != '' THEN
        -- If search text is provided, search with conditions
        SELECT 
            gudang_id,
            gudang_nama,
            gudang_alamat,
            gudang_status
        FROM gudang
        WHERE 
            gudang_status = 'Aktif' AND
            (LOWER(gudang_nama) LIKE CONCAT('%', LOWER(p_cariData), '%') OR
             LOWER(gudang_alamat) LIKE CONCAT('%', LOWER(p_cariData), '%'));
    ELSE
        -- If no search text, retrieve all records
        SELECT 
            gudang_id,
            gudang_nama,
            gudang_alamat,
            gudang_status
        FROM gudang
        WHERE gudang_status = 'Aktif';
    END IF;
END//
DELIMITER ;

-- Dumping structure for procedure db_pos_uas.updateGudang
DROP PROCEDURE IF EXISTS `updateGudang`;
DELIMITER //
CREATE PROCEDURE `updateGudang`(
	IN `p_gudang_id` INT,
	IN `p_gudang_nama` VARCHAR(225),
	IN `p_gudang_alamat` VARCHAR(225),
	IN `p_gudang_status` VARCHAR(225)
)
BEGIN
UPDATE gudang
    SET
        gudang_nama = p_gudang_nama,
        gudang_alamat = p_gudang_alamat,
        gudang_status = p_gudang_status
    WHERE
        gudang_id = p_gudang_id;
END//
DELIMITER ;

/*!40103 SET TIME_ZONE=IFNULL(@OLD_TIME_ZONE, 'system') */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
