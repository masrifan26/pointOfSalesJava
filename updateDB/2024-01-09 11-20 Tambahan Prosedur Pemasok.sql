-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.4.27-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             12.5.0.6677
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for db_pos_uas
DROP DATABASE IF EXISTS `db_pos_uas`;
CREATE DATABASE IF NOT EXISTS `db_pos_uas` /*!40100 DEFAULT CHARACTER SET armscii8 COLLATE armscii8_bin */;
USE `db_pos_uas`;

-- Dumping structure for procedure db_pos_uas.insertPemasok
DROP PROCEDURE IF EXISTS `insertPemasok`;
DELIMITER //
CREATE PROCEDURE `insertPemasok`(
	IN `p_pemasok_nama` VARCHAR(225),
	IN `p_pemasok_alamat` VARCHAR(225),
	IN `p_pemasok_tlp` VARCHAR(30),
	IN `p_pemasok_status` VARCHAR(225)
)
BEGIN
	INSERT INTO pemasok (pemasok_nama, pemasok_alamat, pemasok_tlp, pemasok_status)
   VALUES (p_pemasok_nama, p_pemasok_alamat, p_pemasok_tlp, p_pemasok_status);
END//
DELIMITER ;

-- Dumping structure for procedure db_pos_uas.searchPemasok
DROP PROCEDURE IF EXISTS `searchPemasok`;
DELIMITER //
CREATE PROCEDURE `searchPemasok`(
	IN `p_cariData` VARCHAR(225)
)
BEGIN
	IF p_cariData IS NOT NULL AND p_cariData != '' THEN
        -- If search text is provided, search with conditions
        SELECT 
            pemasok_id,
            pemasok_nama,
            pemasok_alamat,
            pemasok_tlp,
            pemasok_status
        FROM pemasok
        WHERE 
            pemasok_status = 'Aktif' AND
            (LOWER(pemasok_nama) LIKE CONCAT('%', LOWER(p_cariData), '%') OR
             LOWER(pemasok_alamat) LIKE CONCAT('%', LOWER(p_cariData), '%') OR
             LOWER(pemasok_tlp) LIKE CONCAT('%', LOWER(p_cariData), '%'));
    ELSE
        -- If no search text, retrieve all records
        SELECT 
            pemasok_id,
            pemasok_nama,
            pemasok_alamat,
            pemasok_tlp,
            pemasok_status
        FROM pemasok
        WHERE pemasok_status = 'Aktif';
    END IF;
END//
DELIMITER ;

-- Dumping structure for procedure db_pos_uas.updatePemasok
DROP PROCEDURE IF EXISTS `updatePemasok`;
DELIMITER //
CREATE PROCEDURE `updatePemasok`(
	IN `p_pemasok_nama` VARCHAR(225),
	IN `p_pemasok_alamat` VARCHAR(225),
	IN `p_pemasok_tlp` VARCHAR(20),
	IN `p_pemasok_status` VARCHAR(225),
	IN `p_pemasok_id` INT
)
BEGIN
	UPDATE pemasok
    SET
        pemasok_nama = p_pemasok_nama,
        pemasok_alamat = p_pemasok_alamat,
        pemasok_tlp = p_pemasok_tlp,
        pemasok_status = p_pemasok_status
    WHERE
        pemasok_id = p_pemasok_id;
END//
DELIMITER ;

/*!40103 SET TIME_ZONE=IFNULL(@OLD_TIME_ZONE, 'system') */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
