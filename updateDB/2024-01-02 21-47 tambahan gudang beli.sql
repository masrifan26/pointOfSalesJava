-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.38-log - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             12.0.0.6468
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for db_pos_uas
CREATE DATABASE IF NOT EXISTS `db_pos_uas` /*!40100 DEFAULT CHARACTER SET armscii8 COLLATE armscii8_bin */;
USE `db_pos_uas`;

-- Dumping structure for table db_pos_uas.gudang
CREATE TABLE IF NOT EXISTS `gudang` (
  `gudang_id` int(11) NOT NULL AUTO_INCREMENT,
  `gudang_nama` varchar(50) COLLATE armscii8_bin DEFAULT NULL,
  `gudang_alamat` varchar(50) COLLATE armscii8_bin DEFAULT NULL,
  `gudang_status` enum('Aktif','Tidak Aktif') COLLATE armscii8_bin DEFAULT NULL,
  PRIMARY KEY (`gudang_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=armscii8 COLLATE=armscii8_bin;

-- Dumping data for table db_pos_uas.gudang: ~3 rows (approximately)
INSERT INTO `gudang` (`gudang_id`, `gudang_nama`, `gudang_alamat`, `gudang_status`) VALUES
	(1, 'Gudang Utama', 'Jl Sukolilo', 'Aktif'),
	(2, 'Gudang Retail', 'Jl Keputih', 'Aktif');

-- Dumping structure for table db_pos_uas.hak_akses
CREATE TABLE IF NOT EXISTS `hak_akses` (
  `hak_akses_id` int(11) NOT NULL AUTO_INCREMENT,
  `hak_akses_nama` varchar(50) COLLATE armscii8_bin DEFAULT NULL,
  `hak_akses_status` enum('Aktif','Tidak Aktif') COLLATE armscii8_bin DEFAULT NULL,
  PRIMARY KEY (`hak_akses_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=armscii8 COLLATE=armscii8_bin;

-- Dumping data for table db_pos_uas.hak_akses: ~2 rows (approximately)
INSERT INTO `hak_akses` (`hak_akses_id`, `hak_akses_nama`, `hak_akses_status`) VALUES
	(1, 'Administrator', 'Aktif'),
	(2, 'Kasir', 'Aktif');

-- Dumping structure for table db_pos_uas.kartu_stok
CREATE TABLE IF NOT EXISTS `kartu_stok` (
  `kstok_id` int(11) NOT NULL AUTO_INCREMENT,
  `kstok_jenis` enum('masuk','keluar') COLLATE armscii8_bin DEFAULT NULL,
  `kstok_nomor_faktur` char(50) COLLATE armscii8_bin DEFAULT NULL,
  `kstok_detail_faktur_id` int(11) DEFAULT NULL,
  `kstok_produk_id` int(11) DEFAULT NULL,
  `kstok_satuan_id` int(11) DEFAULT NULL,
  `kstok_jumlah` float DEFAULT NULL,
  `kstok_gudang_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`kstok_id`),
  KEY `penjualan_id` (`kstok_detail_faktur_id`),
  KEY `pembelian_id` (`kstok_produk_id`),
  KEY `kstok_nomor_faktur` (`kstok_nomor_faktur`),
  CONSTRAINT `FK_kartu_stok_penjualan_produk` FOREIGN KEY (`kstok_produk_id`) REFERENCES `penjualan_detail` (`pjual_detail_produk_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=armscii8 COLLATE=armscii8_bin;

-- Dumping data for table db_pos_uas.kartu_stok: ~2 rows (approximately)
INSERT INTO `kartu_stok` (`kstok_id`, `kstok_jenis`, `kstok_nomor_faktur`, `kstok_detail_faktur_id`, `kstok_produk_id`, `kstok_satuan_id`, `kstok_jumlah`, `kstok_gudang_id`) VALUES
	(1, 'masuk', 'PJ0010', 30, 3, 1, 12, 1),
	(2, 'masuk', 'PJ0003', 31, 3, 1, 1, 1),
	(6, 'masuk', 'PJ0005', 35, 2, 1, 1, 1),
	(7, 'masuk', 'PJ0011', 36, 3, 1, 1, 1);

-- Dumping structure for table db_pos_uas.karyawan
CREATE TABLE IF NOT EXISTS `karyawan` (
  `karyawan_id` int(11) NOT NULL AUTO_INCREMENT,
  `karyawan_nama` varchar(50) COLLATE armscii8_bin DEFAULT NULL,
  `karyawan_alamat` varchar(50) COLLATE armscii8_bin DEFAULT NULL,
  `karyawan_tlp` char(50) COLLATE armscii8_bin DEFAULT NULL,
  `karyawan_jenis_kelamin` enum('L','P') COLLATE armscii8_bin DEFAULT NULL,
  `karyawan_status` enum('Aktif','Tidak Aktif') COLLATE armscii8_bin DEFAULT NULL,
  PRIMARY KEY (`karyawan_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=armscii8 COLLATE=armscii8_bin;

-- Dumping data for table db_pos_uas.karyawan: ~4 rows (approximately)
INSERT INTO `karyawan` (`karyawan_id`, `karyawan_nama`, `karyawan_alamat`, `karyawan_tlp`, `karyawan_jenis_kelamin`, `karyawan_status`) VALUES
	(1, 'Abdul Rohman Masrifan', 'Jombang', '087863975153', 'L', 'Aktif'),
	(2, 'Labib Falah Athallah', 'Surabaya', '087815964509', 'L', 'Aktif'),
	(3, 'M.Aprilian Rizal', 'Tuban', '088225066808', 'L', 'Aktif'),
	(4, 'Okky Hendrawan', 'Surabaya', '082132269596', 'L', 'Aktif');

-- Dumping structure for table db_pos_uas.pelanggan
CREATE TABLE IF NOT EXISTS `pelanggan` (
  `pelanggan_id` int(10) NOT NULL AUTO_INCREMENT,
  `pelanggan_nama` varchar(50) COLLATE armscii8_bin DEFAULT NULL,
  `pelanggan_alamat` varchar(50) COLLATE armscii8_bin DEFAULT NULL,
  `pelanggan_tlp` char(50) COLLATE armscii8_bin DEFAULT NULL,
  `pelanggan_jenis_kelamin` enum('L','P') COLLATE armscii8_bin DEFAULT NULL,
  `pelanggan_status` enum('Aktif','Tidak Aktif') COLLATE armscii8_bin DEFAULT NULL,
  PRIMARY KEY (`pelanggan_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=armscii8 COLLATE=armscii8_bin;

-- Dumping data for table db_pos_uas.pelanggan: ~2 rows (approximately)
INSERT INTO `pelanggan` (`pelanggan_id`, `pelanggan_nama`, `pelanggan_alamat`, `pelanggan_tlp`, `pelanggan_jenis_kelamin`, `pelanggan_status`) VALUES
	(1, 'Pelanggan Umum', 'Tuban', '088834934', 'L', 'Aktif'),
	(2, 'Labib Surabaya', 'Surabaya', '088834934', 'L', 'Aktif');

-- Dumping structure for table db_pos_uas.pemasok
CREATE TABLE IF NOT EXISTS `pemasok` (
  `pemasok_id` int(11) NOT NULL AUTO_INCREMENT,
  `pemasok_nama` varchar(50) COLLATE armscii8_bin DEFAULT NULL,
  `pemasok_alamat` varchar(50) COLLATE armscii8_bin DEFAULT NULL,
  `pemasok_tlp` char(20) COLLATE armscii8_bin DEFAULT NULL,
  `pemasok_status` enum('Aktif','Tidak Aktif') COLLATE armscii8_bin DEFAULT NULL,
  PRIMARY KEY (`pemasok_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=armscii8 COLLATE=armscii8_bin;

-- Dumping data for table db_pos_uas.pemasok: ~0 rows (approximately)
INSERT INTO `pemasok` (`pemasok_id`, `pemasok_nama`, `pemasok_alamat`, `pemasok_tlp`, `pemasok_status`) VALUES
	(1, 'Rizal Tuban', 'Tuban', '0082382343', 'Aktif'),
	(2, 'www', 'www', '02934', 'Aktif');

-- Dumping structure for table db_pos_uas.pembelian
CREATE TABLE IF NOT EXISTS `pembelian` (
  `pembelian_id` int(11) NOT NULL AUTO_INCREMENT,
  `pembelian_nomor_faktur` char(50) COLLATE armscii8_bin DEFAULT NULL,
  `pembelian_tanggal` date DEFAULT NULL,
  `pembelian_pemasok_id` int(11) DEFAULT NULL,
  `pembelian_gudang_id` int(11) DEFAULT NULL,
  `pembelian_total_rp` float DEFAULT NULL,
  PRIMARY KEY (`pembelian_id`),
  KEY `pemasok_id` (`pembelian_pemasok_id`),
  KEY `pembelian_gudang_id` (`pembelian_gudang_id`),
  CONSTRAINT `pemasok_id` FOREIGN KEY (`pembelian_pemasok_id`) REFERENCES `pemasok` (`pemasok_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=armscii8 COLLATE=armscii8_bin;

-- Dumping data for table db_pos_uas.pembelian: ~0 rows (approximately)

-- Dumping structure for table db_pos_uas.pembelian_detail
CREATE TABLE IF NOT EXISTS `pembelian_detail` (
  `pbeli_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `pbeli_detail_pembelian_id` int(11) DEFAULT NULL,
  `pbeli_detail_gudang_id` int(11) DEFAULT NULL,
  `pbeli_detail_produk_id` int(11) DEFAULT NULL,
  `pbeli_detail_satuan_id` int(11) DEFAULT NULL,
  `pbeli_detail_harga` float DEFAULT NULL,
  `pbeli_detail_jumlah` float DEFAULT NULL,
  `pbeli_detail_diskon_rp` float DEFAULT NULL,
  `pbeli_detail_persen` float DEFAULT NULL,
  `pbeli_detail_subtotal` float DEFAULT NULL,
  PRIMARY KEY (`pbeli_detail_id`),
  KEY `produk_id` (`pbeli_detail_produk_id`),
  KEY `satuan_id` (`pbeli_detail_satuan_id`),
  CONSTRAINT `produk_id` FOREIGN KEY (`pbeli_detail_produk_id`) REFERENCES `produk` (`produk_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `satuan_id` FOREIGN KEY (`pbeli_detail_satuan_id`) REFERENCES `produk_satuan` (`satuan_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=armscii8 COLLATE=armscii8_bin;

-- Dumping data for table db_pos_uas.pembelian_detail: ~0 rows (approximately)

-- Dumping structure for table db_pos_uas.penjualan
CREATE TABLE IF NOT EXISTS `penjualan` (
  `penjualan_id` int(11) NOT NULL AUTO_INCREMENT,
  `penjualan_nomor_faktur` char(50) COLLATE armscii8_bin DEFAULT NULL,
  `penjualan_tanggal` date DEFAULT NULL,
  `penjualan_pelanggan_id` int(11) DEFAULT NULL,
  `penjualan_gudang_id` int(11) DEFAULT NULL,
  `penjualan_total_rp` float DEFAULT NULL,
  `penjualan_bayar_rp` float DEFAULT NULL,
  `penjualan_cara_bayar` enum('Tunai','Kartu','Transfer') COLLATE armscii8_bin DEFAULT NULL,
  `penjualan_kembalian_rp` float DEFAULT NULL,
  `penjualan_keterangan` varchar(250) COLLATE armscii8_bin DEFAULT NULL,
  PRIMARY KEY (`penjualan_id`),
  KEY `pelanggan_id` (`penjualan_pelanggan_id`),
  KEY `penjualan_gudang_id` (`penjualan_gudang_id`),
  CONSTRAINT `pelanggan_id` FOREIGN KEY (`penjualan_pelanggan_id`) REFERENCES `pelanggan` (`pelanggan_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=armscii8 COLLATE=armscii8_bin;

-- Dumping data for table db_pos_uas.penjualan: ~10 rows (approximately)
INSERT INTO `penjualan` (`penjualan_id`, `penjualan_nomor_faktur`, `penjualan_tanggal`, `penjualan_pelanggan_id`, `penjualan_gudang_id`, `penjualan_total_rp`, `penjualan_bayar_rp`, `penjualan_cara_bayar`, `penjualan_kembalian_rp`, `penjualan_keterangan`) VALUES
	(1, 'PJ0001', '2023-12-07', 1, NULL, 200000, 200000, 'Tunai', 0, 'Tes'),
	(2, 'PJ0002', '2023-12-07', 2, NULL, 25000, 100000, 'Kartu', 50000, 'Teman'),
	(6, 'PJ0003', '2023-12-09', 1, 1, 25000, 200000, 'Tunai', 175000, 'wwww'),
	(7, 'PJ0004', '2024-01-01', 1, 1, 120000, 1000, 'Tunai', 0, ''),
	(8, 'PJ0005', '2024-01-01', 1, 1, 120000, 10, 'Tunai', 0, ''),
	(9, 'PJ0006', '2024-01-01', 1, 1, 120000, 0, 'Tunai', 0, 'www123'),
	(10, 'PJ0007', '2024-01-01', 2, 2, 1000, 0, 'Tunai', 0, 'qweqwe'),
	(11, 'PJ0008', '2024-01-01', 1, 1, 601000, 620000, 'Tunai', 19000, 'qweqwe'),
	(12, 'PJ0009', '2024-01-01', 1, 1, 76000, 80000, 'Tunai', 4000, ''),
	(13, 'PJ0010', '2024-01-01', 2, 1, 300000, 300000, 'Tunai', 0, 'www123'),
	(14, 'PJ0011', '2024-01-02', 2, 1, 25000, 30000, 'Tunai', 5000, '');

-- Dumping structure for table db_pos_uas.penjualan_detail
CREATE TABLE IF NOT EXISTS `penjualan_detail` (
  `pjual_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `pjual_detail_penjualan_id` int(11) DEFAULT NULL,
  `pjual_detail_produk_id` int(11) DEFAULT NULL,
  `pjual_detail_satuan_id` int(11) DEFAULT NULL,
  `pjual_detail_harga` float DEFAULT NULL,
  `pjual_detail_jumlah` float DEFAULT NULL,
  `pjual_detail_diskon_rp` float DEFAULT NULL,
  `pjual_detail_diskon_persen` float DEFAULT NULL,
  `pjual_detail_subtotal` float DEFAULT NULL,
  PRIMARY KEY (`pjual_detail_id`),
  KEY `p_detail_id` (`pjual_detail_produk_id`),
  KEY `pjual_detail_pelanggan_id` (`pjual_detail_penjualan_id`) USING BTREE,
  CONSTRAINT `FK_penjualan_detail_produk` FOREIGN KEY (`pjual_detail_produk_id`) REFERENCES `produk` (`produk_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `pjual_penjualan_id` FOREIGN KEY (`pjual_detail_penjualan_id`) REFERENCES `penjualan` (`penjualan_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=armscii8 COLLATE=armscii8_bin;

-- Dumping data for table db_pos_uas.penjualan_detail: ~9 rows (approximately)
INSERT INTO `penjualan_detail` (`pjual_detail_id`, `pjual_detail_penjualan_id`, `pjual_detail_produk_id`, `pjual_detail_satuan_id`, `pjual_detail_harga`, `pjual_detail_jumlah`, `pjual_detail_diskon_rp`, `pjual_detail_diskon_persen`, `pjual_detail_subtotal`) VALUES
	(1, 1, 1, 1, 200000, 1, 0, 0, 200000),
	(12, 2, 3, 1, 25000, 5, 0, 0, 25000),
	(17, 10, 5, 1, 1000, 1, 0, 0, 1000),
	(18, 11, 2, 1, 120000, 5, 0, 0, 600000),
	(19, 11, 5, 1, 1000, 1, 0, 0, 1000),
	(28, 11, 5, 1, 1000, 1, 0, 0, 1000),
	(30, 13, 3, 1, 25000, 12, 0, 0, 300000),
	(31, 6, 3, 1, 25000, 1, 0, 0, 25000),
	(35, 8, 2, 1, 120000, 1, 0, 0, 120000),
	(36, 14, 3, 1, 25000, 1, 0, 0, 25000);

-- Dumping structure for table db_pos_uas.produk
CREATE TABLE IF NOT EXISTS `produk` (
  `produk_id` int(11) NOT NULL AUTO_INCREMENT,
  `produk_kode` varchar(50) COLLATE armscii8_bin NOT NULL DEFAULT '0',
  `produk_nama` varchar(50) COLLATE armscii8_bin DEFAULT NULL,
  `produk_keterangan` varchar(100) COLLATE armscii8_bin DEFAULT NULL,
  `produk_kategori_id` int(11) DEFAULT NULL,
  `produk_status` enum('Aktif','Tidak Aktif') COLLATE armscii8_bin DEFAULT NULL,
  PRIMARY KEY (`produk_id`),
  KEY `FK_produk_produk_kategori` (`produk_kategori_id`),
  CONSTRAINT `kategori_id` FOREIGN KEY (`produk_kategori_id`) REFERENCES `produk_kategori` (`kategori_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=armscii8 COLLATE=armscii8_bin;

-- Dumping data for table db_pos_uas.produk: ~4 rows (approximately)
INSERT INTO `produk` (`produk_id`, `produk_kode`, `produk_nama`, `produk_keterangan`, `produk_kategori_id`, `produk_status`) VALUES
	(1, 'CT001', 'Cat tembok2 Nip', 'wwq12', 2, 'Aktif'),
	(2, 'RK016', 'Cat tembok', 'wwq12', 3, 'Aktif'),
	(3, 'RK015', 'Surya', 'Surya 12 GGaa2', 3, 'Aktif'),
	(5, 'BG001', 'Kursi Lipat', 'wwww', 1, 'Aktif');

-- Dumping structure for table db_pos_uas.produk_detail
CREATE TABLE IF NOT EXISTS `produk_detail` (
  `pdetail_id` int(11) NOT NULL AUTO_INCREMENT,
  `pdetail_produk_id` int(11) DEFAULT NULL,
  `pdetail_satuan_id` int(11) DEFAULT NULL,
  `pdetail_harga` float DEFAULT NULL,
  `pdetail_nilai` float DEFAULT NULL,
  `pdetail_default` enum('Ya','Tidak') COLLATE armscii8_bin DEFAULT NULL,
  PRIMARY KEY (`pdetail_id`),
  KEY `satuan_id_fk` (`pdetail_satuan_id`),
  CONSTRAINT `satuan_id_fk` FOREIGN KEY (`pdetail_satuan_id`) REFERENCES `produk_satuan` (`satuan_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=armscii8 COLLATE=armscii8_bin;

-- Dumping data for table db_pos_uas.produk_detail: ~5 rows (approximately)
INSERT INTO `produk_detail` (`pdetail_id`, `pdetail_produk_id`, `pdetail_satuan_id`, `pdetail_harga`, `pdetail_nilai`, `pdetail_default`) VALUES
	(1, 1, 1, 1000, 1, 'Ya'),
	(18, 3, 1, 25000, 1, 'Ya'),
	(21, 5, 1, 1000, 1, 'Ya'),
	(24, 2, 1, 5000, 1, 'Tidak'),
	(25, 2, 3, 120000, 12, 'Ya');

-- Dumping structure for table db_pos_uas.produk_kategori
CREATE TABLE IF NOT EXISTS `produk_kategori` (
  `kategori_id` int(11) NOT NULL AUTO_INCREMENT,
  `kategori_kode` char(5) COLLATE armscii8_bin DEFAULT NULL,
  `kategori_nama` varchar(50) COLLATE armscii8_bin DEFAULT NULL,
  `kategori_status` enum('Aktif','Tidak Aktif') COLLATE armscii8_bin DEFAULT NULL,
  PRIMARY KEY (`kategori_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=armscii8 COLLATE=armscii8_bin;

-- Dumping data for table db_pos_uas.produk_kategori: ~2 rows (approximately)
INSERT INTO `produk_kategori` (`kategori_id`, `kategori_kode`, `kategori_nama`, `kategori_status`) VALUES
	(1, 'BG', 'Bangunan', 'Aktif'),
	(2, 'CT', 'Cat', 'Aktif'),
	(3, 'RK', 'Rokok', 'Aktif'),
	(4, 'ww', 'wempak', 'Aktif');

-- Dumping structure for table db_pos_uas.produk_satuan
CREATE TABLE IF NOT EXISTS `produk_satuan` (
  `satuan_id` int(11) NOT NULL AUTO_INCREMENT,
  `satuan_kode` char(5) COLLATE armscii8_bin DEFAULT NULL,
  `satuan_nama` varchar(100) COLLATE armscii8_bin DEFAULT NULL,
  `satuan_status` enum('Aktif','Tidak Aktif') COLLATE armscii8_bin DEFAULT NULL,
  PRIMARY KEY (`satuan_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=armscii8 COLLATE=armscii8_bin;

-- Dumping data for table db_pos_uas.produk_satuan: ~4 rows (approximately)
INSERT INTO `produk_satuan` (`satuan_id`, `satuan_kode`, `satuan_nama`, `satuan_status`) VALUES
	(1, 'PCS', 'Pieces', 'Aktif'),
	(2, 'LSN', 'Lusin', 'Aktif'),
	(3, 'BOX', 'BOX', 'Aktif'),
	(4, 'ROLL', 'ROLL', 'Aktif');

-- Dumping structure for table db_pos_uas.user
CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_karyawan_id` int(11) DEFAULT NULL,
  `user_username` varchar(50) COLLATE armscii8_bin DEFAULT NULL,
  `user_password` varchar(50) COLLATE armscii8_bin DEFAULT NULL,
  `user_akses_id` int(11) DEFAULT NULL,
  `user_status` enum('Aktif','Tidak Aktif') COLLATE armscii8_bin DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  KEY `karyawan_id` (`user_karyawan_id`),
  KEY `hak_akses_id` (`user_akses_id`),
  CONSTRAINT `hak_akses_id` FOREIGN KEY (`user_akses_id`) REFERENCES `hak_akses` (`hak_akses_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `karyawan_id` FOREIGN KEY (`user_karyawan_id`) REFERENCES `karyawan` (`karyawan_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=armscii8 COLLATE=armscii8_bin;

-- Dumping data for table db_pos_uas.user: ~4 rows (approximately)
INSERT INTO `user` (`user_id`, `user_karyawan_id`, `user_username`, `user_password`, `user_akses_id`, `user_status`) VALUES
	(1, 1, 'rohman', 'e10adc3949ba59abbe56e057f20f883e', 1, 'Aktif'),
	(2, 2, 'labib', 'e10adc3949ba59abbe56e057f20f883e', 2, 'Aktif'),
	(3, 3, 'rizal', 'e10adc3949ba59abbe56e057f20f883e', 1, 'Aktif'),
	(4, 4, 'okky', 'e10adc3949ba59abbe56e057f20f883e', 1, 'Aktif');

-- Dumping structure for trigger db_pos_uas.penjualan_detail_aft_insert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `penjualan_detail_aft_insert` AFTER INSERT ON `penjualan_detail` FOR EACH ROW BEGIN
	
	INSERT INTO kartu_stok

		(kstok_jenis, kstok_nomor_faktur, kstok_detail_faktur_id, kstok_produk_id, kstok_satuan_id, kstok_jumlah, kstok_gudang_id)  

	SELECT 

		'keluar', m.penjualan_nomor_faktur, d.pjual_detail_id, d.pjual_detail_produk_id, d.pjual_detail_satuan_id, d.pjual_detail_jumlah, m.penjualan_gudang_id

	FROM penjualan_detail d

	LEFT JOIN penjualan m ON (m.penjualan_id = d.pjual_detail_penjualan_id)

	WHERE 

		d.pjual_detail_id = NEW.pjual_detail_id; 	

END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger db_pos_uas.penjualan_detail_aft_update
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `penjualan_detail_aft_update` AFTER UPDATE ON `penjualan_detail` FOR EACH ROW BEGIN

	DELETE FROM kartu_stok WHERE kstok_detail_faktur_id = OLD.pjual_detail_id;
	
	INSERT INTO kartu_stok

		(kstok_jenis, kstok_nomor_faktur, kstok_detail_faktur_id, kstok_produk_id, kstok_satuan_id, kstok_jumlah, kstok_gudang_id)  

	SELECT 

		'keluar', m.penjualan_nomor_faktur, d.pjual_detail_id, d.pjual_detail_produk_id, d.pjual_detail_satuan_id, d.pjual_detail_jumlah, m.penjualan_gudang_id

	FROM penjualan_detail d

	LEFT JOIN penjualan m ON (m.penjualan_id = d.pjual_detail_penjualan_id)

	WHERE 

		d.pjual_detail_id = NEW.pjual_detail_id; 	

END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

/*!40103 SET TIME_ZONE=IFNULL(@OLD_TIME_ZONE, 'system') */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
